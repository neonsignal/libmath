// Copyright 2021 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
module;
#include <cmath>
export module vfir;

namespace math
{
// variable rate FIR filter
// calculates the Euler backward approximation of a second order filter
export
template<typename real> class variable_fir
{
public:
	// create a filter
	// cutoff: cutoff frequency of the filter (Hz)
	// zeta: filter zeta (= 1/Q) (default is critically damped Butterworth)
	explicit variable_fir(real cutoff, real zeta = ::sqrt(2))
	: zeta(zeta)
	{
		constexpr real tau{::acos(-1)*2};
		iw = 1 / (cutoff * tau);
	}

	// push a point through the filter
	// dt: delta time from previous sample
	// p: value
	// return filtered value
	real operator()(real dt, real x)
	{
		if (dt <= 0)
			return x0f;

		real iwdt = iw/dt;
		real dx = (x - x0f + x1f*iwdt*iw) / (1 + zeta*iwdt + iwdt*iwdt);
		x1f = dx / dt;
		return x0f += dx;
	}
private:
	// filter configuration
	real iw{}, zeta{};

	// filtered first and second order state
	real x0f{}, x1f{};
};
}
