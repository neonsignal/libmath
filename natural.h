// Copyright 1998-2016 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once

/** @file natural.h
	natural number functions
	*/
// include files
#include <cstdint>
#include <cstddef>
#include <vector>

namespace math
{
/** Natural numbers. */
struct N
{
	/** word of arbitrary length number */
	using atom = uint32_t;

	/** double word of arbitrary length number */
	using atom2 = uint64_t;

	/** binary representation, low word first. */
	std::vector<atom> value;
public:
	/** addition. */
	N &operator+=(const N &rhs);

	/** addition. */
	friend N operator+(N lhs, const N &rhs) {return lhs += rhs;}

	/** subtraction. */
	N &operator-=(const N &rhs);

	/** subtraction. */
	friend N operator-(N lhs, const N &rhs) {return lhs -= rhs;}

	/** multiplication. */
	N &operator*=(const N &rhs);

	/** multiplication. */
	friend N operator*(N lhs, const N &rhs) {return lhs *= rhs;}
};
}
