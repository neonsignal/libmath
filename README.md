collection of discrete numerical functions including harmonic transforms, convolution and correlation, smoothing and filtering, Kalman filtering, least squares fitting, pseudo-random numbers, CRC calculation, zeta function, quaternion arithmetic, matrix arithmetic, and matrix decomposition

Copyright 1998-2022 Glenn McIntosh

licensed under the GNU General Public License version 3, see [LICENSE.md](LICENSE.md)
