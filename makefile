# definitions
SRC = accumulate butterworth vfir median quaternion transform smooth random filter ntt natural cobs bbp atan pow10
STRIP=strip
CPPFLAGS = -O2 @gcc.conf -MMD -MP -DNDEBUG
LFLAGS = -O2

# executable recipes
%.elf: %.o
	$(CXX) $(LFLAGS) $+ -o $@ && $(STRIP) $@

# generic targets
.PHONY: all clean
all: test.out
clean:
	rm -f $(SRC:%=%.d) $(SRC:%=%.o) $(LIB).a test.elf

# specific targets
test.elf: $(SRC:%=%.o) -l gtest
test.out: test.elf
	./test.elf >/tmp/$$PPID.out && diff -s test.out /tmp/$$PPID.out

# external libraries dummy target
.SECONDARY: -l gtest
-l gtest:

#dependencies
-include $(SRC:%=%.d)
