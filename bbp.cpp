// digit extraction calculation
// Copyright 2024 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
module;
#include <cstdint>
export module bbp;

// Plouffe BBP formula for pi 
// sum (4/(8n+1) - 2/(8n+4) - 1/(8n+5) - 1/(8n+6)) * 16^-n
// does not do error estimate
// digits: number of binary digits in result (up to 32)
// d: binary digit offset (multiple of 4)
// return: binary digits
export
template<int digits>
int bbp(uint32_t d)
{
	// modulo division to calculate remainder at a particular digit place
	auto powmoddiv = [](uint32_t p, uint64_t r, const uint32_t m) -> uint64_t
	{
		uint32_t r0{16};
		while (true)
		{
			if (p%2)
				r = r*r0 % m;
			if (!(p /= 2))
				return ((r<<32)/m << 32) + ((r<<32)%m << 32) / m;  // divide 96/32->64 bits
			r0 = uint64_t(r0)*r0 % m;
		}
	};

	// sum up all terms to left of the selected digit
	d /= 4;
	uint64_t s{};
	for (uint32_t i = 0; i <= d; ++i)
	{
		s += powmoddiv(d-i, 4, 8*i+1);
		s -= powmoddiv(d-i, 2, 8*i+4);
		s -= powmoddiv(d-i, 1, 8*i+5);
		s -= powmoddiv(d-i, 1, 8*i+6);
	}

	// add a few terms to the right of the selected digit
	uint64_t r{1ull<<64-4};
	for (uint32_t i = d+1; i < d+7; ++i)
	{
		s += r*4 / (8*i+1);
		s -= r*2 / (8*i+4);
		s -= r*1 / (8*i+5);
		s -= r*1 / (8*i+6);
		r /= 16;
	}

	// correction for average truncation of divisions
	s -= d+7;

	// extract digit
	return s >> sizeof(s)*8-digits;
}
