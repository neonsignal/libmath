// Copyright 2022-2023 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
module;
#include <cmath>
export module accumulate;

// compensated summation
// reduces numerical error in floating-point accumulation

namespace math
{
// Kahan-Babushka method
export
template<typename T>
class KahanSum
{
public:
	// current value
	// return: sum
	operator T() const {return sum;}
public:
	// accumulate
	// x: value to be accumulated
	// return: sum
	T operator+=(T x)
	{
		x += delta;
		T sum1{sum+x};
		delta = x - (sum1-sum);
		return sum = sum1;
	}
public:
	T sum{}, delta{};
};

// Kahan method with Neumaier enhancement
// handles input that are large compared to sum
export
template<typename T = float>
class KahanNeumaierSum
{
public:
	// current value
	// return: sum
	operator T() const {return sum;}
public:
	// accumulate
	// x: value to be accumulated
	// return: sum
	T operator+=(T x)
	{
		T sum1{sum+delta+x};
		delta += fabs(x) < fabs(sum) ?  x-(sum1-sum) : sum+(x-sum1);
		return sum = sum1;
	}
public:
	T sum{}, delta{};
};
}
