// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// running median filter
module;
#include <algorithm>
export module median;

namespace math
{
// running median filter
// N: size of filter
// T: type of elements
// M: selected percentile point 0..N-1 (default median)
export
template<int N, typename T, int M=N/2> class median
{
public:
	// initialize indices
	median()
	{
		for (int i = 0; i < N; ++i)
		{
			values[i] = 0;
			index[i] = heaps[i] = i;
		}
	};

	// filter
	// x: input value
	// return: median
	T operator()(T x)
	{
		constexpr int lower{0}, lower_end{M}, median{M}, upper{1+M}, upper_end{N};

		// remove oldest element
		int i = index[i_index];

		// rebalance, leaving median empty
		if (i != median)
		{
			if (i < upper)
				while (i > lower)
					i = exchange(i, (i-lower)/2+lower);
			else
				while (i > upper)
					i = exchange(i, (i-upper)/2+upper);
			i = exchange(i, median);
		}

		// place new element in median
		values[i_index] = x;
		index[i_index] = i;
		heaps[i] = i_index;
		i_index = (i_index+1)%N;

		// rebalance
		// lower heap ascending, upper heap descending
		if (x < values[heaps[lower]])
		{
			// insert and rebalance lower heap
			int j{lower};
			while (i != j)
			{
				i = exchange(i, j);
				int k;
				k = i*2+(1-lower);
				if (k < lower_end && values[heaps[k]] > values[heaps[j]])
					j = k;
				k = i*2+(2-lower);
				if (k < lower_end && values[heaps[k]] > values[heaps[j]])
					j = k;
			}
		}
		else if (x > values[heaps[upper]])
		{
			// insert and rebalance upper heap
			int j{upper};
			while (i != j)
			{
				i = exchange(i, j);
				int k;
				k = i*2+(1-upper);
				if (k < upper_end && values[heaps[k]] < values[heaps[j]])
					j = k;
				k = i*2+(2-upper);
				if (k < upper_end && values[heaps[k]] < values[heaps[j]])
					j = k;
			}
		}

		// return median
		return values[heaps[median]];
	}
private:
	// exchange entries
	// i: current entry
	// j: next entry
	// return: next entry
	int exchange(int i, const int j)
	{
		using std::swap;
		swap(index[heaps[i]], index[heaps[j]]);
		swap(heaps[i], heaps[j]);
		return j;
	}
private:
	int i_index{0};  // circular pointer
	T values[N];  // values
	int index[N];  // index into heaps
	int heaps[N];  // median and heaps
};
}
