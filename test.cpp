// Copyright 1998-2016 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

// mathematical function library test file

// include files
#include <iostream>
#include <iomanip>
#include <chrono>
#include <random>
#include <complex>
#include <deque>
#include "linear.h"
#include "matrix.h"
#include "kalman.h"
#include "quaternion.h"
#include "transform.h"
import accumulate;
#include "smooth.h"
import butterworth;
import vfir;
import median;
#include "zeta.h"
#include "random.h"
#include "lsq.h"
#include "filter.h"
#include "ntt.h"
#include "natural.h"
#include "erasure.h"
import cobs;
import bbp;
import atan;
using std::cout, std::cerr, std::endl, std::hex, std::dec;
using std::setw;
using std::complex;

// output
template<size_t N, size_t M> void display(math::Matrix<N, M> x)
{
	for (int i = 0; i < ssize(x); i++)
	{
		for (int j = 0; j < ssize(x[i]); j++)
			cout << x[i][j] << ' ';
		cout << endl;
	}
}
void display(const math::Vector &x)
{
	for (int i = 0; i < ssize(x); i++)
		cout << x[i] << ' ';
	cout << endl;
}
void display(const math::Vector2D &x)
{
	for (int i = 0; i < ssize(x); i++)
	{
		for (int j = 0; j < ssize(x[i]); j++)
			cout << x[i][j] << ' ';
		cout << endl;
	}
}
void display(const math::Mpint &x)
{
	for (int i = 0; i < ssize(x); i++)
		cout << x[i] << ' ';
	cout << endl;
}

// test routine
void test()
{
	try
	{
		math::Vector a(128, 0.0);

		// initial data
		math::UniformRandom x(1);
		for (int i = 0; i < ssize(a); ++i)
			a[i] = float{x} * 100;
		cout << "*** vector data" << endl;
		display(a);

		// test correlation
		cout << "*** correlation" << endl;
		math::Vector c = a;
		math::autoCorrelate(c);
		display(c);

		// test wavelet transform
		cout << "*** Haar wavelet transform data" << endl;
		math::haarTransform0(a);
		display(a);

		// test inverse wavelet transform
		cout << "*** inverse Haar wavelet transform data" << endl;
		math::haarTransform1(a);
		display(a);

		// test hartley transform
		cout << "*** Hartley transform data" << endl;
		math::hartley(a);
		display(a);

		// test inverse hartley transform
		cout << "*** inverse Hartley transform data" << endl;
		math::hartley(a);
		display(a);

		// test smooth
		math::Vector b;
		cout << "*** polynomial smoothed data" << endl;
		b = a;
		auto fir = math::savitsky_golay_filter<10, 2>(0);
		math::Vector response{fir.begin(), fir.end()};
		math::fir_smooth(b, response, true);
		display(b);
		cout << "*** Gaussian smoothed data" << endl;
		b = a;
		math::Vector xx(b.size(), 0.0);
		for (int i = 0; i < ssize(xx); ++i)
			xx[i] = i;
		math::gaussian_smooth(xx, b, 5.);
		display(b);
		cout << "*** Butterworth filtered data" << endl;
		math::butterworth<2> butterworth({16.944,-6.120});
		for (int i = 0; i < ssize(a); ++i)
			b[i] = butterworth(a[i]);
		display(b);
		cout << "*** variable rate FIR filtered data" << endl;
		math::variable_fir variable_fir(1.);
		for (int i = 0; i < ssize(a); ++i)
			b[i] = variable_fir(1/10., a[i]);
		display(b);
		cout << "*** median filter" << endl;
		math::median<5, math::real> median;
		for (int i = 0; i < ssize(a); ++i)
			b[i] = median(a[i]);
		display(b);

		// test least squares curve fit
		cout << "*** least squares curve fit" << endl;
		b = a;
		math::Lsq<2> lsq;
		lsq.add(xx, b);
		lsq.fit();
		lsq.evaluate(xx);
		display(xx);

		// test zeta function
		cout << "*** Zeta ***" << endl;
		Zeta<24> zeta;
		cout << zeta(0.0) << endl;
		cout << zeta(0.5) << endl;
		cout << zeta(1.0) << endl;
		cout << zeta(1.5) << endl;
		cout << zeta(8.0) << endl;
		cout << zeta(-1.0) << endl;
		cout << zeta(-2.0) << endl;
		cout << zeta(-7.0) << endl;
		cout << zeta(-8.0) << endl;
		cout << zeta(complex{0.5, 0.5}) << endl;
		cout << zeta(complex{0.5, 13.0}) << endl;
		cout << zeta(complex{0.5, -21.022040}) << endl;
		cout << zeta(complex{0.5, -25.010858}) << endl;

		// test random number generators
		cout << "*** hashing and random number generators ***" << endl;
		cout << hex;
		math::UniformRandom r0(1);
		float{r0};
		unsigned long int h = r0.Hash(1);
		cout << h << ' ' << float{r0} << endl;
		h = r0.Hash(99);
		r0.SetIndex(99);
		cout << h << ' ' << float{r0} << endl;
		math::UniformRandom r1(99);
		float{r1};
		h = r1.Hash(1);
		cout << h << ' ' << float{r1} << endl;
		h = r1.Hash(99);
		r1.SetIndex(99);
		cout << h << ' ' << float{r1} << endl;
		math::RandomBitSequence r2(0xBAA96887);
		for (int i = 0; i < 64; ++i)
			cout << (r2?'1':'0');
		cout << endl;
		cout << dec;

		// test summation
		b = a;
		math::KahanSum<math::real> sum1;
		for (auto x: b)
			sum1 += x;
		for (auto x: b)
			sum1 += -x;
		cout << sum1 << endl;
		math::KahanNeumaierSum<math::real> sum2;
		for (auto x: b)
			sum2 += x;
		for (auto x: b)
			sum2 += -x;
		cout << sum2 << endl;

		// test filter
		cout << "*** IIR filtering ***" << endl;
		math::IirFilter f0(4);
		f0 = 345;
		for (int i = 0; i < 30; ++i)
			cout << (f0 += 346) << ' ';
		cout << endl;

		// initial data
		math::Matrix<3,3> m{};
		for (int i = 0; i < ssize(m); ++i)
			for (int j = 0; j < ssize(m[0]); ++j)
				m[i][j] = float{x} * 100;
		cout << "*** matrix data" << endl;
		display(m);

		// invert data
		cout << "*** multiplied by inversion" << endl;
		display(math::operator*(m, math::invert(m)));

		// normalize rotation matrix
		cout << "*** normalize rotation matrix" << endl;
		display(math::normalizeRotationMatrix(m));

		// square of cholesky decomposition
		{
			// initial data
			math::Matrix<4,4> m{};
			m[0][0] =  5.0; m[0][1] =  1.2; m[0][2] =  0.3; m[0][3] = -0.6;
			m[1][0] =  1.2; m[1][1] =  6.0; m[1][2] = -0.4; m[1][3] =  0.9;
			m[2][0] =  0.3; m[2][1] = -0.4; m[2][2] =  8.0; m[2][3] =  1.7;
			m[3][0] = -0.6; m[3][1] =  0.9; m[3][2] =  1.7; m[3][3] = 10.0;
			cout << "*** matrix data" << endl;
			display(m);

			// cholesky decomposition
			math::choleskyDecompose(m);
			cout << "*** cholesky decomposition" << endl;
			display(math::operator*(m, math::transpose(m)));
		}

		// test number theoretic transform
		{
			math::Mpint a(128);

			// initial data
			math::UniformRandom x(1);
			for (int i = 0; i < ssize(a); ++i)
				a[i] = (uint32_t) x & 256-1;
			cout << "*** vector data" << endl;
			display(a);

			// test number theoretic transform transform
			cout << "*** number theoretic transform" << endl;
			math::ntt<>(a);
			display(a);

			// test inverse number theoretic transform transform
			cout << "*** inverse number theoretic transform" << endl;
			math::intt<>(a);
			display(a);
		}

		// test natural number transformations
		{
			cout << "*** arbitrary precision multiplication ***" << endl;
			math::N x{{0xFFFF0000, 0x12345678}}, y{{0xFFFF1234, 0x12345678}};
			math::N z = x*y;
			for (int i = 0; i < ssize(z.value); ++i)
				cout << hex << z.value[i] << dec;
			cout << endl;
		}

		// test erasure codes
		{
			// initialize data set
			constexpr int n_bytes{12};
			struct packet {uint8_t bytes[n_bytes];};
			constexpr int n_packets{16};
			uint8_t data[n_packets*n_bytes], recovered_data[n_packets*n_bytes];
			std::default_random_engine generator(std::random_device{}());
			generator.seed(1);
			std::uniform_int_distribution<> distribution(0, 255);
			for (int i = 0; i < n_packets*n_bytes; ++i)
				data[i] = distribution(generator);

			// display data set
			cout << "*** packet data" << endl;
			cout << std::setfill('0');
			for (int i = 0; i < n_packets; ++i)
			{
				for (int j = 0; j < n_bytes; ++j)
					cout << hex << setw(2) << int{data[i*n_bytes+j]} << dec;
				cout << endl;
			}
			cout << std::setfill(' ');

			// run test
			math::RemainderErasureTransmit<packet, 32> construct;
			math::RemainderErasureReceive<packet, 32> reconstruct;
			//math::LubyTransformTransmit<packet, 32> construct;
			//math::LubyTransformReceive<packet, 32> reconstruct;
			int transmit = 0;
			while (transmit < 100)
			{
				// construct packet
				packet packet = {0};
				int seed = generator()%2048;
				construct(seed, sizeof(data)/sizeof(packet), packet, &data);

				// send selection
				++transmit;
				if (reconstruct(seed, sizeof(recovered_data)/sizeof(packet), packet, &recovered_data))
					break;
			}

			// display data set
			cout << "*** remainder erasure code reconstructed data" << endl;
			cout << "packets=" << transmit << endl;
			cout << std::setfill('0');
			for (int i = 0; i < n_packets; ++i)
			{
				for (int j = 0; j < n_bytes; ++j)
					cout << hex << setw(2) << int{recovered_data[i*n_bytes+j]} << dec;
				cout << endl;
			}
			cout << std::setfill(' ');
		}

		// test COBS
		{
			std::deque<uint8_t> buffer;

			// output streamer
			auto output = [&buffer](uint8_t c)
			{
				buffer.push_back(c);
			};

			// input streamer
			auto input = [&buffer]() -> uint8_t
			{
				auto c = buffer.front();
				buffer.pop_front();
				return c;
			};

			// COBS streams
			Cobs cobs(output);
			Decobs decobs(input);

			// test each packet
			auto test = [&cobs, &decobs, &buffer](uint8_t *test, int n_test)
			{
				// test COBS encoding
				// input from test array, output to buffer
				cobs(test, n_test);
				cobs();
				for (int i = 0; i < std::ssize(buffer); ++i)
					cout << setw(2) << std::setfill('0') << hex << (unsigned int) buffer[i] << dec;
				// test COBS decoding
				// input from buffer, output to result 
				uint8_t result[1000];
				int n_result = decobs(result);
				if (n_result != n_test)
					cout << " length changed after decode";
				for (int i = 0; i < n_result; ++i)
					if (result[i] != test[i])
					{
						cout << " byte error after decode";
						break;
					}
				std::cout << std::endl;
			};

			cout << "*** COBS byte stuffing" << endl;
			uint8_t b0[] {};
			test(b0, sizeof(b0));
			uint8_t b1[] {0x00};
			test(b1, sizeof(b1));
			uint8_t b2[] {0x00,0x00};
			test(b2, sizeof(b2));
			uint8_t b3[] {0x00,0x11,0x00};
			test(b3, sizeof(b3));
			uint8_t b4[] {0x11,0x22,0x00,0x33};
			test(b4, sizeof(b4));
			uint8_t b5[] {0x11,0x22,0x33,0x44};
			test(b5, sizeof(b5));
			uint8_t b6[] {0x11,0x00,0x00,0x00};
			test(b6, sizeof(b6));
			uint8_t b7[] {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,0xCE,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,};
			test(b7, sizeof(b7));
			uint8_t b8[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,0xCE,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,};
			test(b8, sizeof(b8));
			uint8_t b9[] {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,0xCE,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0xFF};
			test(b9, sizeof(b9));
			uint8_t bA[] {0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,0xCE,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0xFF,0x00};
			test(bA, sizeof(bA));
			uint8_t bB[] {0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,0xCE,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0xFF,0x00,0x01};
			test(bB, sizeof(bB));
		}

		// test BBP
		{
			cout << "*** BBP pi calculation" << endl;
			for (int d = 0*4; d < 8*4; d += 16)
				cout << setw(4) << hex << bbp<16>(d) << dec;
			cout << "...";
			for (int d = 490723*4; d < 490731*4; d += 16)
				cout << setw(4) << hex << bbp<16>(d) << dec;
			cout << "...";
			for (int d = 501436*4; d < 501444*4; d += 16)
				cout << setw(4) << hex << bbp<16>(d) << dec;
			cout << endl;
		}

		// speed trials
		//#define SPEEDTRIAL
		#ifdef SPEEDTRIAL

		// hartley transforms
		cerr << "*** hartley transforms" << endl;
		cerr << "length, MFLOPS" << endl;
		for (int n = 16; n <= 1*1024*1024; n *= 2)
		{
			math::Vector a(n);
			math::UniformRandom x(1);
			for (int i = 0; i < n; ++i)
				a[i] = float{x} * 100;
			int repeats = 1024*1024/n;
			if (repeats == 0) repeats = 1;
			auto t0 = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < repeats; ++i)
			{
				math::hartley(a);
				math::hartley(a);
			}
			auto t1 = std::chrono::high_resolution_clock::now();
			cerr << n << ", " << 2.5*n*log2(n)/1e6/(std::chrono::duration<double>(t1-t0).count()/2/repeats) << endl;
		}

		// multiprecision multiplication
		cerr << "*** arbitrary precision multiplication" << endl;
		cerr << "length, MFLOPS" << endl;
		for (int n = 16; n <= 1*1024*1024; n *= 2)
		{
			math::Mpint a(n);
			math::UniformRandom x(1);
			for (int i = 0; i < n; ++i)
				a[i] = (uint32_t) x & 16-1;
			int repeats = 1024*1024/n;
			if (repeats == 0) repeats = 1;
			auto t0 = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < repeats; ++i)
			{
				math::ntt<>(a);
				math::intt<>(a);
			}
			auto t1 = std::chrono::high_resolution_clock::now();
			cerr << n << ", " << 2.5*n*log2(n)/1e6/(std::chrono::duration<double>(t1-t0).count()/2/repeats) << endl;
		}

		// erasure codes
		{
			cerr << "*** Chinese remainder erasure code" << endl;
			// initialize data set
			constexpr int n_bytes{12};
			struct packet {uint8_t bytes[n_bytes];};
			constexpr int n_packets{16};
			uint8_t data[n_packets*n_bytes], recovered_data[n_packets*n_bytes];
			std::default_random_engine generator(std::random_device{}());
			generator.seed(1);
			std::uniform_int_distribution<> distribution(0, 255);
			for (int i = 0; i < n_packets*n_bytes; ++i)
				data[i] = distribution(generator);

			// test measurements
			constexpr int n_repeats{100000};
			int sum{0};
			int max{0};

			// run test
			auto t0 = std::chrono::high_resolution_clock::now();
			for (int repeat = 0; repeat < n_repeats; ++repeat)
			{
				math::RemainderErasureTransmit<packet, 32> construct;
				math::RemainderErasureReceive<packet, 32> reconstruct;
				//math::LubyTransformTransmit<packet, 32> construct;
				//math::LubyTransformReceive<packet, 32> reconstruct;
				int transmit = 0;
				while (transmit < 200)
				{
					// construct message
					packet message = {0};
					int seed = generator()%2048;
					construct(seed, sizeof(data)/sizeof(message), message, &data);

					// send selection
					++transmit;
					if (reconstruct(seed, sizeof(recovered_data)/sizeof(message), message, &recovered_data))
						break;
				}

				sum += transmit;
				if (transmit > max) max = transmit;

				// check result
				for (int i = 0; i < n_packets*n_bytes; ++i)
					if (recovered_data[i] != data[i])
					{
						cerr << "error: at repeat " << repeat << ' ' << transmit << endl;
						repeat = n_repeats;
						break;
					}
			}
			auto t1 = std::chrono::high_resolution_clock::now();
			cerr << "time/packet=" << 1e06 * std::chrono::duration<double>(t1-t0).count()/n_repeats/n_packets << "us" << ' ' << "average_sent=" << 1.*sum/n_repeats << ' ' << "maximum_sent=" << max << endl;
		}
		#endif // SPEEDTRIAL
	}
	catch (std::exception &error)
	{
		cerr << "exception " << error.what() << endl;
	}
}

// main
int main(int argc, char **argv)
{
	// flags
	int iArg = 1;
	char *arg = argv[1];
	while (iArg < argc && (arg[0] == '-' || arg[0] == '/'))
	{
		char c = tolower(arg[1]);
		switch (c)
		{
		case '?': case 'h':
		default:
			cerr << "Usage(V1.0): " << argv[0] << " [-options] filename" << endl;
			cerr << "   -h	 : help" << endl;
			return 1;
		}
		arg = argv[++iArg];
	}

	// files
	while (iArg < argc)
	{
		arg = argv[++iArg];
	}
	if (iArg < argc)
	{
		cerr <<"Error: too many arguments" << endl;
		return 1;
	}

	// test function
	test();
}
