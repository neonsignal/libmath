// Copyright 2024 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
export module pow10;

// integer power of 10 (compile time)
export template<typename T, T i> constexpr T pow10() {return pow10<T, i-1>()*10;}
template<> constexpr int pow10<int, 0>() {return 1;}
template<> constexpr unsigned int pow10<unsigned int, 0>() {return 1;}
