// Copyright 1998-2016 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once

/** @file random.h
	Random number generation.
	*/

// include files
#include <cstdint>

namespace math
{
/** Random number generator. */
class UniformRandom
{
public:
	/** Create random number object.
		@param seed seed value for random sequence
		*/
	UniformRandom(uint32_t seed) : mSeed(seed) {}

	/** Reset index into random sequence.
		@param index index of next element to be generated
		*/
	void SetIndex(uint32_t index)
	{
		mIndex = index;
	}

	/** Return random number.
		@return uniform random deviate in the range [0,MAXUINT)
		*/
	operator uint32_t();

	/** Return random number.
		@return uniform random deviate in the range [0.0,1.0)
		*/
	operator float();

	/** Hash a value.
		@param input value to be hashed
		@return hashed value
		*/
	uint32_t Hash(uint32_t input);

private:
	UniformRandom() {}
	uint32_t mSeed, mIndex{0};
};

/** Random bit generator. */
class RandomBitSequence
{
public:
	/** Create random number object.
		@param seed seed value for random bit sequence (>0)
		*/
	RandomBitSequence(uint32_t seed) : mBuffer(seed) {}

	/** Return random bit.
		@return bit
		*/
	operator bool();
private:
	RandomBitSequence() {}
	uint32_t mBuffer;
};
}
