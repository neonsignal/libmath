// COBS byte stuffing, packet to byte-serial format
// Copyright 2023 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
module;
#include <cstdint>
export module cobs;

// COBS encoding
export
template<typename T>
class Cobs
{
	// default COBS delimiter is a NUL
	static constexpr uint8_t delimiter{0};
public:
	// constructor
	// output: character output function
	Cobs(T output) : output(output) {}

	// encoding data
	// buffer: pointer to input character buffer
	// n: length of input buffer
	// output: function to stream output bytes
	void operator()(const uint8_t *buffer, int n)
	{
		for (int i = 0; i < n; ++i)
			encode(buffer[i]);
	}

	// encoding end of packet
	// output: function to stream output bytes
	void operator()()
	{
		flush();
		output(delimiter);
	}
private:
	// encode character
	// c: input character
	// output: function to stream output bytes
	void encode(uint8_t c)
	{
		if (buffer[0] == 0)
			++buffer[0];
		if (c == 0)
		{
			flush();
			++buffer[0];
			return;
		}
		buffer[buffer[0]] = c;
		if (++buffer[0] == 255)
			flush();
	}

	// packet flush
	// output: function to stream output bytes
	void flush()
	{
		buffer[0] ^= delimiter;
		for (int i = 0; i < buffer[0]; ++i)
			output(buffer[i]);
		buffer[0] = 0;
	}
private:
	T output;
	uint8_t buffer[255] {0};
};

// COBS decoding
export
template<typename T>
class Decobs
{
	// default COBS delimiter is a NUL
	static constexpr uint8_t delimiter{0};
public:
	// constructor
	// input: character output function
	Decobs(T input) : input(input) {}

	// decoding data
	// buffer: pointer to output character buffer
	int operator()(uint8_t *buffer)
	{
		int i{0};
		while (!decode(buffer+i))
			++i;
		return i;
	}
private:
	// decode a character
	// p_c: pointer to character
	// return: 0=character 1=end -1=error
	bool decode(uint8_t *p_c)
	{
		if (i == 0)
		{
			if (flush() == 0)
				return 1;
			++i;
		}
		if (i == skip)
		{
			if (flush() == 0)
				return 1;
			*p_c = 0;
			++i;
			return 0;
		}
		uint8_t v = input();
		if (v == delimiter)  // unexpected input
		{
			i = 0;
			return -1;
		}
		*p_c = v;
		if (++i == 255)
			i = 0;
		return 0;
	}

	// read in the next flush size
	// return: size
	int flush()
	{
		skip = input()^delimiter;
		i = 0;
		return skip;
	}
private:
	T input;
	int i{0}, skip{};
};
