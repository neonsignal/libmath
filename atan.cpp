// Copyright 2024 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
module;
#include <numbers>
export module atan;

// fast approximation to atan2
// maximum of ~4deg error
// maintains sort order for angles
template<typename T>
const T atan2_fast(T x, T y) {return copysign(1-x/(fabs(x)+fabs(y)), y)*(std::numbers::pi/2);}
