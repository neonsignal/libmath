// Copyright 1998-2016 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

// include files
#include "ntt.h"

namespace math
{
// useful moduli
// order, scale, root
// 1u<<30, {3u}, {5^3}
// 1u<<28, {13u}, {3^13}
// 1u<<27, {15u,17u,29u}, {31^15, 3^17, 3^29}
// 1u<<26, {7u,27u,37u,43u}, {3^7, 13^27, 3^37, 3^43}
}
