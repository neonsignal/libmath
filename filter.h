// Copyright 1998-2016 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once

/** @file filter.h
	Signal filtering
	*/

// include files
#include <cstdint>

namespace math
{
/** IIR filter. */
class IirFilter
{
public:
	//! Create filter.
	IirFilter() {}

	//! Create filter.  @param shift 1<<shift is proportion of current value to new
	IirFilter(uint8_t shift) : mShift(shift) {}

	/** Set current value.
		@param value input value
		*/
	IirFilter &operator=(int32_t value)
	{
		mValue = value<<mShift;
		return *this;
	}

	/** Supply another value to the filter
		@param value input value
		*/
	IirFilter &operator+=(int32_t value)
	{
		mValue -= mValue>>mShift;
		mValue += value;
		return *this;
	}

	/** Return the value from the filter
		*/
	operator int32_t()
	{
		return mValue>>mShift;
	}

private:
	int32_t mValue{0};
	uint8_t mShift{0};
};
}
