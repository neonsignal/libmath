// Copyright 2021 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
module;
#include <utility>
export module butterworth;

namespace math
{
// digital Butterworth FIR filter
// poles: number of poles
export
template<int poles> class butterworth
{
public:
	// initialize coefficients
	// yf: y coefficients (relative to x)
	// see https://www.meme.net.au/butterworth.html
	// scaling is calculated internally
	explicit butterworth(const double (&yf)[poles], double y_init = 0)
	{
		scale = 1 << poles;
		for (int i = 0; i < poles; ++i)
		{
			scale += this->yf[i] = yf[i];
		}
		for (int i = 0; i < poles; ++i)
		{
			x0[i] = y_init;
			y0[i] = y_init;
		}
	}

	// push a point through the filter
	// x: value
	// return filtered value
	double operator()(double x)
	{
		using std::swap;

		for (int i = 0; i < poles; ++i)
		{
			swap(x, x0[i]);
			x += x0[i];
		}
		for (int i = 0; i < poles; ++i)
		{
			x += yf[i]*y0[i];
		}
		x /= scale;
		for (int i = 0; i < poles; ++i)
		{
			swap(x, y0[i]);
		}
		return y0[0];
	}
 private:
	double scale{1 << poles}, yf[poles]{};
	double x0[poles]{}, y0[poles]{};
};
}
